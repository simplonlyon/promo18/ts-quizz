# Approfondissement du TypeScript

## Exercice : création d'un quizz

### Objectifs

1. Consolider les apprentissages concernant la syntaxe du TypeScript
2. Accéder au DOM pour récupérer des valeurs
3. Gérer les événements JS
4. Manipuler la POO en TS
5. Manipuler le DOM en modifiant la structure du document par code


### Partie 1 : Structure de la page

Créez une page HTML avec un titre, une zone de questionnaire, et une zone de résultat en-dessous.  
Vous pouvez la faire responsive si vous volez mais ce n'est pas le but de l'exercice.

### Partie 2 : POO

  - Créez le type "Question" (id, String intitulé, String réponsejuste, String[] réponsesfausses, String explication, booléen répondu)

  - Créez le type Quizz (String joueur, Question[] lstquestions, int score)

  - dans vore main.ts, créez un quizz, et 3 questions manuellement. Une question a 1 réponse juste, et une liste de réponses fausses

### Partie 3 : Fonctionnalités

  - Quand la page s'affiche, affichez dans la zone de questionnaire, la première question, sous forme de formulaire : l'intitulé de la question en titre, chaque réponse (la juste et les fausses) sous forme de bouton radio, un bouton de validation
  - Quand l'utilisateur clique sur le bouton de validation:
    - le bouton de validation disparaît
    - mettre la bonne réponse en gras et en vert
    - si la réponse choisie est la bonne, augmenter le score de 1 (dans la classe Quizz et à l'écran)
    - si la réponse choisie est mauvaise, la faire apparaître en rouge
    - sous la liste de réponses, faire apparaître la justification, et un bouton pour passer à la question suivante
    - quand l'utilisateur clique sur le bouton "question suivante", la question disparaît, remplacée par la question suivante
  - Quand toutes les questions ont été répondues, le questionnaire disparaît et le score apparaît en grand à la place

### Bonus

  - Au début du quizz, avant l'affichage de la première question, demander le nom du joueur
  - Afficher les questions et leurs réponses dans un ordre aléatiore
  - Afficher un chronomètre global pour le quizz
  - Afficher un compte à rebours de 20 secondes sur la question, quand il tombe à 0 c'est comme si l'utilisateur n'avait pas choisi de réponse