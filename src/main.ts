import './style.css'
import { Question, Quizz } from './entities';

const score = document.querySelector<HTMLDivElement>('#score')!
let questionToDisplay = 0;

let q = new Question(
  1,
  "Met-on du sucre dans de la béchamel ?",
  "Non",
  ["Oui", "Peut-être, si on a envie"],
  "La béchamel ne requiert pas de sucre, mais du sel !"
);
let q2 = new Question(
  2,
  "Quelle est la forme du ballon au rugby ?",
  "Ovale",
  ["Ronde"],
  "Le rugby est surnommé \"Le monde de l'ovalie\" car son ballon est de forme ovale, contrairement à d'autres sports"
);

let q3 = new Question(
  3,
  "Comment s'appelle la norme qui régit le stockage de données de santé en France ?",
  "HDS",
  ["HIPAA"],
  "En France, la norme qui régit le stockage de données de santé en France est la HDS, pour Hébergement de Données de Santé. La HIPAA est une norme américaine."
);

let quizz = new Quizz("Player 1", [q, q2, q3]);


function buildAnswer(id:string, name:string, answer:string) : HTMLElement[] {
  let radio = document.createElement("input");
  radio.setAttribute("type", "radio");
  radio.value = answer;
  radio.name = name;
  radio.id = id;
  let label = document.createElement("label");
  label.textContent = answer;
  label.setAttribute("for", radio.id);
  return [radio, label];
}

function buildQuestion(question:Question) : HTMLFormElement {
  let f = document.createElement("form");
  // Création du titre
  let h2 = document.createElement("h2");
  h2.textContent = question.label;
  f.appendChild(h2);


  let idAnswer = 0;
  /* La bonne réponse : on va devoir créer :
    - un bouton radio
    - un label pour faire apparaître le texte

    Rappels : 
      - l'attribut "for" du label doit indiquer l'id du bouton
      - les radios partagent tous le même nom pour avoir un choix unique
  */
  let answerElements = buildAnswer(idAnswer.toString(), question.id.toString(), question.trueAnswer);
  f.appendChild(answerElements[0]);
  f.appendChild(answerElements[1]);
  f.appendChild(document.createElement("br"));
  idAnswer++;

  /* Les mauvaises réponses : même chose que pour la bonne réponse, mais dans une boucle */
  for(let answer of question.falseAnswers) {
    let answerElements = buildAnswer(idAnswer.toString(), question.id.toString(), answer);
    f.appendChild(answerElements[0]);
    f.appendChild(answerElements[1]);
    f.appendChild(document.createElement("br"));
    idAnswer++;
  }
  
  // Bouton de validation
  let submit = document.createElement("input");
  submit.setAttribute("type", "submit");
  submit.value = "Valider";
  submit.addEventListener('click', (event) => {
    event.preventDefault();
    console.log("Bouton de validation cliqué");
    // Disparition du bouton submit
    f.removeChild(submit);
    // Mettre en vert et en gras la bonne réponse
    document.querySelector("#questionnaire label")?.classList.add("ok");
    // Check de la réponse donnée
    let givenAnswer = <HTMLInputElement> document.querySelector("input:checked");
    if (givenAnswer) {
      //  Si bonne réponse : score +1
      if (question.trueAnswer == givenAnswer.value) {
        quizz.score++;
        score.textContent = quizz.score.toString();
      } else { //  Sinon, afficher en rouge
        givenAnswer.nextElementSibling?.classList.add("bad");
      }
    }

    // Affichage de l'explication
    let explanation = document.createElement("p");
    explanation.textContent = question.explanation;
    f.appendChild(explanation);
    
    // Affichage du bouton "Question suivante"
    let button = document.createElement("input");
    button.type = "button";
    button.value = "Question suivante";
    button.addEventListener("click", () => {
      questionToDisplay++;
      displayQuestion(buildQuestion(quizz.lstQuestions[questionToDisplay]));
    })
    f.appendChild(button);
    return false;
  });
  f.appendChild(submit);

  return f;
}

function displayQuestion(f:HTMLFormElement):void {
  const questionnaire = document.querySelector<HTMLDivElement>('#questionnaire')!

  questionnaire.innerHTML = '';
  questionnaire.appendChild(f);
}


displayQuestion(buildQuestion(quizz.lstQuestions[questionToDisplay]));

score.textContent = quizz.score.toString();