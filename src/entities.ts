
export class Question {
    id:number;
    label:string;
    trueAnswer:string;
    falseAnswers:string[];
    explanation:string;
    answered:boolean;

    constructor(id:number, label:string, trueAnswer:string,
            falseAnswers:string[], explanation:string) {
        this.id = id;
        this.label = label;
        this.trueAnswer = trueAnswer;
        this.falseAnswers = falseAnswers;
        this.explanation = explanation;
        this.answered = false;
    }

    setAnswered() : void{
        this.answered = true;
    }
}

export class Quizz {
    player:string;
    lstQuestions:Question[];
    score:number;

    constructor(player:string,lstQuestions:Question[]) {
        this.player  = player;
        this.lstQuestions = lstQuestions;
        this.score = 0;
    }
}